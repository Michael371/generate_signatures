﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Generate_signatures
{
    public class ThreadHash
    {
        private int _l;
        private int _r;
        //private List<byte[]> _lb = null;
        private Stack<byte[]> _lb = null;

        public ThreadHash(int l, int r, Stack<byte[]> lb)
        {
            this._l = l;
            this._r = r;
            this._lb = lb;
        }

        public void PrintHash()
        {
            var mySHA256 = SHA256.Create();

            for (int i = _l; i < _r; ++i)
                Console.WriteLine($"Block: {i}; Thread: {Thread.CurrentThread.ManagedThreadId}; SHA256: {PrintByteArray(mySHA256.ComputeHash(_lb.ElementAt(i)))};");
        }
        private string PrintByteArray(byte[] array)
        {
            int i;
            string output = string.Empty;
            for (i = 0; i < array.Length; i++)
            {
                output += string.Format("{0:X2}", array[i]);
                if ((i % 4) == 3) output += " ";
            }

            return output;
        }
    }
}
