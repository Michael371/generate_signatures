﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading;

namespace Generate_signatures
{
    class Program
    {
        private static string PATH;
        //private const string PATH = @"D:\ФИЛЬМЫ\Treugolnik.2009.RUS.BDRip.XviD.AC3.-HQ-ViDEO.avi";
        //private const string PATH = @"D:\Samples\input_0 (1).png";
        //private const int BLOCK_SIZE_BYTE = 20971520;
        private static int BLOCK_SIZE_BYTE;

        static Stack<byte[]> lb = new Stack<byte[]>();
        static Stopwatch stopWatch = new Stopwatch();

        static void Main(string[] args)
        {

            try
            {
                PATH = args[0];
                BLOCK_SIZE_BYTE = Convert.ToInt32(args[1]);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                return;
            }

            int cores = Environment.ProcessorCount;
            //int cores = 4;
            Thread[] threads = new Thread[cores];
            ThreadHash[] th = new ThreadHash[cores];

            stopWatch.Start();
            try
            {
                using (var reader = new BinaryReader(File.Open(PATH, FileMode.Open, FileAccess.Read)))
                {
                    while (reader.BaseStream.Position != reader.BaseStream.Length)
                    {
                        int size = 0;
                        try
                        {
                            size = (reader.BaseStream.Length - reader.BaseStream.Position < BLOCK_SIZE_BYTE) ?
                            (int)(reader.BaseStream.Length - reader.BaseStream.Position) : BLOCK_SIZE_BYTE;

                            byte[] buf = new byte[size];
                            buf = reader.ReadBytes(size);
                            
                            lb.Push(buf);
                            Console.WriteLine($"File size {reader.BaseStream.Length} - Position {reader.BaseStream.Position}");
                        }
                        catch (OutOfMemoryException e)
                        {
                            Console.WriteLine(e.Message);
                            PrintThreadHash(cores, threads, th);
                        }
                    }
                    PrintThreadHash(cores, threads, th);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                return;
            }
            stopWatch.Stop();

            TimeSpan ts = stopWatch.Elapsed;
            Console.WriteLine(string.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10));

            Console.WriteLine("Done;\n");
            Console.ReadKey();
        }

        private static void PrintThreadHash(int cores, Thread[] threads, ThreadHash[] th)
        {
            var delta = lb.Count / cores;

            for (int i = 0; i < cores; ++i)
            {
                var l = i * delta;
                var r = l + delta;
                th[i] = new ThreadHash(l, r, lb);
            }

            for (var i = 0; i < cores; ++i)
                threads[i] = new Thread(new ThreadStart(th[i].PrintHash));

            for (var i = 0; i < cores; ++i)
                threads[i].Start();

            foreach (var thread in threads)
                while (thread.IsAlive)
                    Thread.Sleep(100);

            lb.Clear();
        }
    }
}
